<?php

$type = isset($_GET['type']) ? $_GET['type'] : null;
$lesson = isset($_GET['lesson']) ? $_GET['lesson'] : null;
if ($type == null && $lesson == null) {
    $type = "all";
}
if ($type != null) {
    $sql = "SELECT * FROM lesson ";
    if ($type == "rzo") {
        $stmt = $bdd->prepare("$sql WHERE ID_THEME = 1");

    } else if ($type == "dev") {
        $stmt = $bdd->prepare("$sql WHERE ID_THEME = 2");


    } else {
        $stmt = $bdd->prepare($sql);

    }

    $stmt->execute();

    $res = $stmt->fetchAll();
    ?>
    <div class="container-fluid d-flex row m-auto">

    <?php
    foreach ($res as $key => $value) {

        ?>
        <div class="card my-2 col-4" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title"><?= $value['ID_THEME'] == 1 ? "RÉSEAU" : "DÉVELOPPEMENT"; ?></h5>
                <h6 class="card-title"><?= $value['TITLE']; ?></h6>
                <p class="card-text col-12 text-truncate" style="max-height: 150px;"><?= $value['CONTENU']; ?></p>
                <a href="?page=lesson&lesson=<?= $value['ID_LESSON']; ?> " class="btn btn-primary">J'veux
                    l'apprendre</a>
            </div>
        </div>
        <?php

    }
    ?>

    </div>
    <?php
}
?>
<?php
if (isset($_GET['lesson']) && $_GET['lesson'] != null && !isset($_GET['type'])) {
    $sql = "SELECT * FROM lesson ";
    $stmt = $bdd->prepare("$sql WHERE ID_LESSON = :p_lessonId");

    $stmt->execute(array(
        "p_lessonId" => $_GET['lesson']
    ));

    $res = $stmt->fetchAll();

    ?>
    <div class="container-fluid py-2">
        <div class="card my-0 col-12 mx-auto" style="width: 75%;">
            <div class="card-body w-100">
                <h5 class="card-title"><?= $res[0]['ID_THEME'] == 1 ? "RÉSEAU" : "DÉVELOPPEMENT"; ?></h5>
                <h6 class="card-title"><?= $res[0]['TITLE']; ?></h6>
                <p class="card-text"><?= $res[0]['CONTENU']; ?></p>
                <a href="?page=eval&lesson=<?= $res[0]['ID_LESSON']; ?>" class="btn btn-primary">Testez moi !</a>
            </div>
        </div>
    </div>
    <?php
}