<div class="d-inline-flex container">
    <div class="position-relative float-left">
        <img src="./res/images/monkey_index1.png" class="w-25 float-left">
        <h3 class="position-relative float-right mt-5">Vous n'y comprenez rien à l'informatique?</h3>
        <br>
        <br>
        <p class="position-relative float-right mx-5">Venez découvrir comment fonctionne votre ordinateur!</p>
    </div>
</div>
<div class="d-inline-flex container border-top w-100">
    <div class="position-relative float-left">
        <img src="./res/images/monkey_index2.png" class="w-25 float-right">
        <h3 class="position-relative float-left mt-5">Vous avez envie de devenir un pro de l'ordinateur?</h3>
        <br>
        <br>
        <p class="position-relative float-left mx-5">Vous allez devenir le meilleur des meilleurs!</p>
    </div>
</div>
<div class="d-inline-flex container border-top">
    <div class="position-relative float-left h-75">
        <img src="./res/images/monkey_index3.png" class="w-25 float-left">
        <h3 class="position-relative float-right mt-5">Le développement ou le réseau vous passionne?</h3>
        <br>
        <br>
        <p class="position-relative float-right mx-5">Alors n'attendez plus et commencez notre formation en ligne 100% gratuite!</p>
    </div>
</div>
<?php


?>

</div>
