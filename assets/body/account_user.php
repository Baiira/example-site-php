<?php
if (isset($_POST['submit']) && $_POST['submit'] == 'oui') {
    $sql = "UPDATE user SET ";
    $array = array();
    if (!empty($_POST['pseudo'])) {
        $sql .= "PSEUDO = :p_pseudo";
        $array["p_pseudo"] = $_POST['pseudo'];
        if (sizeof($_POST) > 1 && (!empty($_POST['password1']) || !empty($_POST['prenom']) || !empty($_POST['nom']))) {
            $sql .= ", ";
        }
    }
    if (!empty($_POST['nom'])) {
        $sql .= "NOM = :p_nom";
        $array["p_nom"] = $_POST['nom'];
        if (sizeof($_POST) > 1 && (!empty($_POST['password1']) || !empty($_POST['prenom']))) {
            $sql .= ", ";
        }
    }
    if (!empty($_POST['prenom'])) {
        $sql .= "PRENOM = :p_prenom";
        $array["p_prenom"] = $_POST['prenom'];
        if (sizeof($_POST) > 1 && !empty($_POST['password1'])) {
            $sql .= ", ";
        }
    }
    if (!empty($_POST['password1']) && $_POST['password1'] == $_POST['password2']) {
        $sql .= "PASSWORD = :p_password ";
        $array["p_password"] = $_POST['password1'];
    }

    $sql .= " WHERE ID_USER = :p_id";
    $array["p_id"] = $_SESSION['id_user'];

    $stmt = $bdd->prepare($sql);

    $stmt->execute($array);

    $_SESSION['pseudo'] = $_POST['pseudo'];
    $_SESSION['nom'] = $_POST['nom'];
    $_SESSION['prenom'] = $_POST['prenom'];

    ?>
    <div class="pt-5">
        <div class="alert alert-success" role="alert">
            Votre profil a bien été modifié !
        </div>
    </div>
    <?php
}
?>

<form action="" method="post">
    <div class="form-group">
        <label for="exampleInputEmail2">Pseudo</label>
        <input type="text" class="form-control" name="pseudo" id="exampleInputEmail2"
               aria-describedby="emailHelp"
               placeholder="xXdark_billy_du_34Xx" value="<?= $_SESSION['pseudo']; ?>" required>
    </div>
    <div class="form-group">
        <label for="exampleInputNom">Nom</label>
        <input type="text" class="form-control" name="nom" id="exampleInputNom"
               aria-describedby="emailHelp"
               placeholder="Noobie" value="<?= $_SESSION['nom']; ?>" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPrenom">Prénom</label>
        <input type="text" class="form-control" name="prenom" id="exampleInputPrenom"
               aria-describedby="emailHelp"
               placeholder="Billy" value="<?= $_SESSION['prenom']; ?>" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword3">Mot de passe</label>
        <input type="password" class="form-control" name="password1" id="exampleInputPassword3"
               placeholder="Mot de passe">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword4">Confirmez votre mot de passe</label>
        <input type="password" class="form-control" name="password2" id="exampleInputPassword4"
               placeholder="Mot de passe">
    </div>
    <button type="submit" name="submit" value="oui" class="btn btn-success">Modifier</button>
</form>