<?php

if (isset($_POST['pseudo']) && !empty($_POST['pseudo'])
    && isset($_POST['password1']) && !empty($_POST['password1'])
    && isset($_POST['password2']) && !empty($_POST['password2'])
    && isset($_POST['nom']) && !empty($_POST['nom'])
    && isset($_POST['prenom']) && !empty($_POST['prenom'])) {

    if ($_POST['password1'] === $_POST['password2']) {
        $stmt = $bdd->prepare("SELECT * FROM user WHERE pseudo = :p_pseudo");
        $stmt->execute(array(
            "p_pseudo" => $_POST['pseudo']
        ));
        $res = $stmt->fetch();
        if ($res[0] === null) {
            $stmt = $bdd->prepare("INSERT INTO user (id_user, pseudo, nom, prenom, password) VALUES (:p_id,  :p_pseudo, :p_nom, :p_prenom, :p_password)");

            $stmt->execute(array(
                "p_id" => null,
                "p_pseudo" => $_POST['pseudo'],
                "p_nom" => $_POST['nom'],
                "p_prenom" => $_POST['prenom'],
                "p_password" => $_POST['password1'],
            ));
        } else {
            ?>
            <div class="pt-4" style="height: 550px;">

                <img src="./res/images/monkey_woups.png" class="w-25"/>

                <div class="float-right">
                    <h1>Woups, </h1>
                    <p>le pseudo que vous avez choisi est déjà pris !</p>
                </div>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="pt-4" style="height: 550px;">

            <img src="./res/images/monkey_woups.png" class="w-25"/>

            <div class="float-right">
                <h1>Woups, </h1>
                <p>les deux mots de passe ne sont pas identiques, veuillez recommencer !</p>
            </div>
        </div>
        <?php

    }
} else {

    ?>
    <div class="pt-4" style="height: 550px;">

        <img src="./res/images/monkey_woups.png" class="w-25"/>

        <div class="float-right">
        <h1>Woups, </h1>
        <p class="float-right">veuillez recommencer, nos braves singes ont peut être fait une bêtise et une
            erreur bizarre a été faite.</p>
        </div>

    </div>
    <?php
}
?>

</div>