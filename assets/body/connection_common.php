<?php

if (isset($_POST['pseudo']) && isset($_POST['password'])) {
    $stmt = $bdd->prepare("SELECT * FROM user WHERE pseudo = :p_pseudo");
    $stmt->execute(array(
        "p_pseudo" => $_POST['pseudo']
    ));
    $res = $stmt->fetch();

    if ($res[0] !== null && $res['PASSWORD'] !== null && $res['PASSWORD'] == $_POST['password']) {
            $_SESSION['id_user'] = $res['ID_USER'];
            $_SESSION['pseudo'] = $res['PSEUDO'];
            $_SESSION['nom'] = $res['NOM'];
            $_SESSION['prenom'] = $res['PRENOM'];
            $_SESSION['access'] = $res['ADMIN'];

            ?>
            <script language="JavaScript">
                document.location.href="./"
            </script>

            <?php
    } else {
        ?>

        <div class="pt-4" style="height: 550px;">

            <img src="./res/images/monkey_banana.png" class="w-25"/>

            <div class="float-right">
                <h1>Woups, </h1>
                <p>Soit votre compte n'existe pas, soit vous vous êtes trompé dans vos identifiants</p>
            </div>
        </div>

        <?php

    }
}