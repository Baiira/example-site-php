<?php
$sql = "SELECT l.title AS title, r.resultat AS resultat, l.id_lesson as idLesson
FROM eval AS e, lesson AS l, resultat AS r, user AS u
WHERE e.id_lesson = l.id_lesson AND r.id_eval = e.id_eval AND u.id_user = :p_idUser AND r.id_user = u.id_user";

$stmt = $bdd->prepare($sql);

$stmt->execute(array(
    "p_idUser" => $_SESSION['id_user']
));

$res = $stmt->fetchAll();

?>
<div class="container-fluid d-flex row m-auto">
    <?php
    foreach ($res as $k => $v) {
        if ($v['resultat'] <= 5 && $v['resultat'] >= 0) {
            $txt = "Vaudrait mieux ressayer !";
            $css = "alert alert-danger";
        } else if ($v['resultat'] <= 10) {
            $txt = "C'est pas encore trop ça !";
            $css = "alert alert-warning";
        } else if ($v['resultat'] <= 15) {
            $txt = "Bien jouer, mais vous pouvez faire mieux !";
            $css = "alert alert-success";
        } else if ($v['resultat'] <= 20) {
            $txt = "Bravo, vous n'avez plus rien à apprendre sur ce cours !";
            $css = "alert alert-primary";
        } else {
            $txt = "Kékicépacé?";
            $css = "alert alert-secondary";
        }
        ?>
        <div class="card my-2 col-4" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title"><?= $v['title']; ?></h5>
                <p class="card-text"><?= $v['resultat']; ?>/20</p>
                <p class="card-text <?= $css; ?>"><?= $txt; ?></p>
                <a href="?page=eval&lesson=<?= $v['idLesson']; ?> " class="btn btn-primary">J'veux
                    recommencer !</a>
            </div>
        </div>
        <?php
    }

    ?>

</div>

