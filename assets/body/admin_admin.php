<div class="h-100 py-5">
    <!-- Nav pills -->
    <ul class="nav nav-pills">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="pill" href="#cours">Cours</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#eval">Eval</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane container active" id="cours">
            <div class="form-group">
                <form action="" method="post">
                    <div class="form-group">
                        <label for="title">Intitulé du cours</label>
                        <input type="text" class="form-control" name="title" placeholder="Titre du cours" required>
                    </div>
                    <div class="form-group">
                        <label for="content">Contenu</label>
                        <textarea name="content" class="form-control" placeholder="Contenu" required></textarea>
                    </div>
                    <label>Thème du cours:</label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="theme" id="exampleRadios1" value="1"
                               checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Réseau
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="theme" id="exampleRadios1" value="2">
                        <label class="form-check-label" for="exampleRadios1">
                            Développement
                        </label>
                    </div>
                    <input type="submit" class="btn btn-success" value="Ajouter">
                </form>
            </div>
        </div>
        <div class="tab-pane container fade" id="eval">
            <div class="form-group">
                <form action="" method="post">
                    <div class="form-group">
                        <label for="lesson">Cours lié</label>
                        <input type="text" class="form-control" name="lesson" placeholder="Titre du cours" required>
                    </div>
                    <div class="form-group">
                        <label for="eval_content">Contenu</label>
                        <textarea name="eval_content" class="form-control" placeholder="Contenu" required></textarea>
                    </div>
                    <input type="submit" class="btn btn-success" value="Ajouter">
                </form>
            </div>
        </div>
    </div>
<?php
if (isset($_SESSION['oui'])) {
    $_SESSION['oui'] = null;
    ?>
    <div class="alert alert-success" role="alert">
        Votre contenu a bien été sauvegardé !
    </div>
    <?php
}
?>
</div>
<?php
if (isset($_POST['title']) && !empty($_POST['title'])
    && isset($_POST['content']) && !empty($_POST['content'])
    && isset($_POST['theme']) && !empty($_POST['theme'])) {

    $stmt = $bdd->prepare("INSERT INTO lesson (id_lesson, title, contenu, id_theme) VALUES (:p_id, :p_title, :p_content, :p_theme)");

    $stmt->execute(array(
        "p_id" => null,
        "p_title" => $_POST['title'],
        "p_content" => $_POST['content'],
        "p_theme" => $_POST['theme'],
    ));
    $_SESSION['oui'] = 'oui';
} else if (isset($_POST['lesson']) && !empty($_POST['lesson'])
    && isset($_POST['eval_content']) && !empty($_POST['eval_content'])) {

    $stmt = $bdd->prepare("INSERT INTO eval (id_eval, contenu, id_lesson) VALUES (:p_id, :p_content, :p_id_lesson)");

    $stmt->execute(array(
        "p_id" => null,
        "p_content" => $_POST['eval_content'],
        "p_id_lesson" => $_POST['lesson'],
    ));

    $_SESSION['oui'] = 'oui';
}