<?php

if (isset($_GET['test']) && $_GET['test'] == "done") {
    $result = 0;
    for ($i = 0; $i < sizeof($_POST) - 1; $i += 2) {
        $x = $i + 1;
        if ($_POST["$i"] == $_POST["$x"]) {
            $result++;
        }
    }
    $result = $result / ($i / 2) * 20;

    $stmt = $bdd->prepare("INSERT INTO resultat (ID_EVAL, ID_USER, ID_RESULTAT, RESULTAT) VALUES (:p_idEval, :p_idUser, NULL, :p_resultat)");

    $stmt->execute(array(
        "p_idUser" => $_SESSION['id_user'],
        "p_idEval" => (int) $_POST['idEval'],
        "p_resultat" => $result,
    ));

    ?>
    <a class="btn btn-success" href="?page=lesson&type=all&lesson=all">Retour aux cours</a>
    <?php
} else if (isset($_GET['lesson']) && $_GET['lesson'] !== null) {

    $stmt = $bdd->prepare("SELECT * FROM eval WHERE id_lesson = :p_idEval");

    $stmt->execute(array(
        "p_idEval" => $_GET['lesson']
    ));

    $res = $stmt->fetchAll();
    $res = $res[0];

    ?>

    <div class="container-fluid py-2">
        <div class="card my-0 col-6 mx-auto" style="width: 75%;">
            <div class="card-body">
                <form class="form-group" method="post" action="?page=eval&lesson=<?= $res['ID_LESSON']; ?>&test=done">
                    <input type="hidden" value="<?= $res['ID_EVAL']; ?>" name="idEval">
                    <p class="card-text"><?= $res['CONTENU']; ?></p>
                    <button type="submit" class="btn btn-success">
                        Je valide !
                    </button>
                </form>
            </div>
        </div>
    </div>

    <?php

} else {
    ?>
    <div class="bg-light h-100 d-flex">
        <img src="./res/images/monkey_error.png" class="position-relative w-25 h-75 m-4 float-left">

        <div class="d-flex justify-content-center m-4 position-relative">
            <h1 class="position-absolute">Woups !</h1>

            <div class="position-relative mt-5">
                <p><br>On dirait que nos braves singes ont fait une bêtise !
                    <br>
                    <br>
                    Nous nous chargeons de les corriger,
                    <br>
                    En attendant pour continuer votre navigation cliquez sur la flèche en en haut à gauche de votre
                    navigateur !</p>
            </div>
        </div>
    </div>
    <?php
}