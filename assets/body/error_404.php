<div class="bg-light h-100 d-flex">
    <img src="./res/images/monkey_error.png" class="position-relative w-25 h-75 m-4 float-left">

    <div class="d-flex justify-content-center m-4 position-relative">
        <h1 class="position-absolute">Woups !</h1>

        <div class="position-relative mt-5">
            <p><br>On dirait que nos braves singes ont fait une bêtise !
                <br>
                <br>
                Nous nous chargeons de les corriger,
                <br>
                En attendant pour continuer votre navigation cliquez sur la flèche en en haut à gauche de votre
                navigateur !</p>
        </div>
    </div>
</div>