<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">
    <link rel="stylesheet" type="text/css" href="./res/bootstrap/css/bootstrap.min.css">
    <link rel="shortcut icon" href="./res/images/monkey_logo.png" type="image/x-icon">
    <title>Notre bô site de e-learning</title>
</head>
<body>
<noscript>
    You need to enable JavaScript to run this app.
</noscript>

<div class="container-fluid alert-primary m-0 w-100">

    <a href="./">
        <img src="./res/images/monkey_logo.png" height="100px" width="100px" class="position-relative m-1"/>
    </a>

    <?php
    ?>

    <nav class="navbar navbar-expand-lg float-right">
        <a class="navbar-brand" href="./">Accueil</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="./?page=account">Mon profil</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Les cours
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="./?page=lesson&lesson=all&type=all">Tous</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="./?page=lesson&lesson=all&type=rzo">Réseau</a>
                        <a class="dropdown-item" href="./?page=lesson&lesson=all&type=dev">Développement</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="./?page=recap">Mon récap</a>
                    </div>
                </li>
            </ul>
            <div class="position-relative float-right m-2">
                <a class="btn btn-danger float-right" href="./?page=disconnect">
                    Se deconnecter
                </a>
            </div>
        </div>
    </nav>
</div>

<div class="container-fluid position-relative mx-auto border-left border-right bg-light" style="width: 75%">
