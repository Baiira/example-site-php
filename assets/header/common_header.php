<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000000">
    <link rel="stylesheet" type="text/css" href="./res/bootstrap/css/bootstrap.min.css">
    <link rel="shortcut icon" href="./res/images/monkey_logo.png" type="image/x-icon">
    <title>Notre bô site de e-learning</title>
</head>
<body>
<noscript>
    You need to enable JavaScript to run this app.
</noscript>

<div class="container-fluid alert-primary m-0 w-100">
    <a href="./">
        <img src="./res/images/monkey_logo.png" height="100px" width="100px" class="position-relative m-1"/>
    </a>
    <div class="position-relative float-right m-2">
        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#modalConnect">
            Se connecter
        </button>
    </div>

    <div class="modal fade" id="modalConnect" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Se connecter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="?page=connection" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pseudo</label>
                            <input type="text" class="form-control" name="pseudo" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   placeholder="xXdark_billy_du_34Xx" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mot de passe</label>
                            <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                                   placeholder="Mot de passe" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Se connecter</button>
                        <button type="button" class="btn btn-primary float-right" data-dismiss="modal"
                                data-toggle="modal" data-target="#modalSubscribe">
                            S'inscrire
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSubscribe" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">S'inscrire</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="?page=subscribe" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail2">Pseudo</label>
                            <input type="text" class="form-control" name="pseudo" id="exampleInputEmail2"
                                   aria-describedby="emailHelp"
                                   placeholder="xXdark_billy_du_34Xx" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputNom">Nom</label>
                            <input type="text" class="form-control" name="nom" id="exampleInputNom"
                                   aria-describedby="emailHelp"
                                   placeholder="Noobie" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPrenom">Prénom</label>
                            <input type="text" class="form-control" name="prenom" id="exampleInputPrenom"
                                   aria-describedby="emailHelp"
                                   placeholder="Billy" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword3">Mot de passe</label>
                            <input type="password" class="form-control" name="password1" id="exampleInputPassword3"
                                   placeholder="Mot de passe" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword4">Confirmez votre mot de passe</label>
                            <input type="password" class="form-control" name="password2" id="exampleInputPassword4"
                                   placeholder="Mot de passe" required>
                        </div>
                        <button type="submit" class="btn btn-primary">S'inscrire</button>
                        <button type="button" class="btn btn-primary float-right" data-dismiss="modal"
                                data-toggle="modal" data-target="#modalConnect">
                            Se connecter
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container-fluid position-relative mx-auto border-left border-right bg-light" style="width: 75%">
<?php

