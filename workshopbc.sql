-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 28 juin 2018 à 15:02
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `workshopbc`
--

-- --------------------------------------------------------

--
-- Structure de la table `eval`
--

DROP TABLE IF EXISTS `eval`;
CREATE TABLE IF NOT EXISTS `eval` (
  `ID_EVAL` int(11) NOT NULL AUTO_INCREMENT,
  `ID_LESSON` int(11) NOT NULL,
  `CONTENU` varchar(5000) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ID_EVAL`,`ID_LESSON`),
  KEY `ID_LESSON` (`ID_LESSON`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `eval`
--

INSERT INTO `eval` (`ID_EVAL`, `ID_LESSON`, `CONTENU`) VALUES
(1, 1, '<div class=\"form-group\">\r\n        <label for=\"exampleInputEmail2\">Quelle est l\'adresse du réseau de l\'adresse IP 11.193.60.10/16 ?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"0\" id=\"exampleInputEmail2\"\r\n               aria-describedby=\"emailHelp\"\r\n               placeholder=\"\" required>\r\n        <input type=\"hidden\" name=\"1\" value=\"11.193.0.0\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"exampleInputNom\">Quelle est l\'adresse du l\'hôte 10.12.192.48/8 ?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"2\" id=\"exampleInputNom\"\r\n               aria-describedby=\"emailHelp\"\r\n               placeholder=\"\" required>\r\n        <input type=\"hidden\" name=\"3\" value=\"0.12.192.48\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"exampleInputPrenom\">Quelle est l\'adresse de broadcast de pour l\'adresse IP 192.168.172.68/24 ?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"4\" id=\"exampleInputPrenom\"\r\n               aria-describedby=\"emailHelp\"\r\n               placeholder=\"\" required>\r\n        <input type=\"hidden\" name=\"5\" value=\"192.168.172.255\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"exampleInputPassword3\">Quelle est l\'adresse du réseau de l\'adresse 172.20.20.32/16 ?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"6\" id=\"exampleInputPassword3\"\r\n               placeholder=\"\" required>\r\n        <input type=\"hidden\" name=\"7\" value=\"172.20.0.0\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"exampleInputPassword4\">Quelle est l\'adresse de la première machine pour l\'adresse 100.50.100.100/19 ?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"8\" id=\"exampleInputPassword4\"\r\n               placeholder=\"\" required>\r\n        <input type=\"hidden\" name=\"9\" value=\"100.50.96.1\">\r\n    </div>'),
(8, 4, '<div class=\"form-group\">\r\n        <label for=\"exampleInputEmail2\">Quelles sont les balises de premier niveau ?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"0\" id=\"exampleInputEmail2\"\r\n               aria-describedby=\"emailHelp\"\r\n               placeholder=\"Separer les reponses par un espace\" required>\r\n        <input type=\"hidden\" name=\"1\" value=\"html head body\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"exampleInputNom\">Quelle balise permet de déclarer du code Javascript?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"2\" id=\"exampleInputNom\"\r\n               aria-describedby=\"emailHelp\"\r\n               placeholder=\"\" required>\r\n        <input type=\"hidden\" name=\"3\" value=\"script\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"exampleInputPrenom\">Quelle balise permet de déclarer du code CSS ?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"4\" id=\"exampleInputPrenom\"\r\n               aria-describedby=\"emailHelp\"\r\n               placeholder=\"\" required>\r\n        <input type=\"hidden\" name=\"5\" value=\"style\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"exampleInputPassword3\">Quelle balise permet de déclarer le titre de la page ?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"6\" id=\"exampleInputPassword3\"\r\n               placeholder=\"\" required>\r\n        <input type=\"hidden\" name=\"7\" value=\"title\">\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label for=\"exampleInputPassword4\">Quelle balise permet de déclarer un formulaire ?</label>\r\n        <input type=\"text\" class=\"form-control\" name=\"8\" id=\"exampleInputPassword4\" required>\r\n        <input type=\"hidden\" name=\"9\" value=\"form\">\r\n    </div>');

-- --------------------------------------------------------

--
-- Structure de la table `lesson`
--

DROP TABLE IF EXISTS `lesson`;
CREATE TABLE IF NOT EXISTS `lesson` (
  `ID_LESSON` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(255) COLLATE utf8_bin NOT NULL,
  `CONTENU` varchar(5000) COLLATE utf8_bin NOT NULL,
  `ID_THEME` int(11) NOT NULL,
  PRIMARY KEY (`ID_LESSON`),
  KEY `ID_THEME` (`ID_THEME`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `lesson`
--

INSERT INTO `lesson` (`ID_LESSON`, `TITLE`, `CONTENU`, `ID_THEME`) VALUES
(1, 'Les masques de sous-réseau', 'Qu’est-ce qu’un masque de sous réseau et à quoi ça sert ? \r\n<br><br><br>\r\nUn masque de sous-réseau est un masque distinguant les bits d\'une adresse IPv4 utilisées pour identifier le sous-réseau de ceux utilisés pour identifier l\'hôte.\r\nExemple : <br>\r\n\r\n192.168.3.1/24<br>\r\n\r\nIci le sous réseau est 192.168.3.0<br>\r\n\r\nEt l’hôte 0.0.0.1<br>\r\n\r\n<br>\r\n<br>\r\nComment calculer le masque ? <br><br>\r\nDans un premier temps il faut convertir l’adresse IP et le masque (donc le /XX) en binaire.\r\nReprenons l’adresse IP 192.168.3.1/24<br>\r\n\r\n192.168.3.1 			1100 0000.1010 1000.0000 0001.0000 0010<br>\r\n\r\n/24 correspond en binaire à 	1111 1111.1111 1111. 1111 1111.0000 0000<br>\r\n\r\nEnsuite nous appliquons l’opérateur logique ET <br>\r\n\r\nPetit rappel :	0 ET 0 = 0<br>\r\n\r\n0 ET 1 = 0<br>\r\n1 ET 0 = 0<br>\r\n\r\n1 ET 1 = 1<br>\r\n\r\n\r\nCe qui nous donne 1100 0000.1010 1000.0000 0001.0000 0000<br>\r\n\r\nQui est égal à 192.168.3.0<br><br>\r\nQu’est-ce qu’un masque de sous réseau et à quoi ça sert ? \r\n<br><br>\r\nUn masque de sous-réseau est un masque distinguant les bits d\'une adresse IPv4 utilisées pour identifier le sous-réseau de ceux utilisés pour identifier l\'hôte.\r\nExemple : <br>\r\n\r\n192.168.3.1/24<br>\r\n\r\nIci le sous réseau est 192.168.3.0<br>\r\n\r\nEt l’hôte 0.0.0.1<br>\r\n\r\n<br><br>\r\n\r\nComment calculer le masque ? <br><br>\r\nDans un premier temps il faut convertir l’adresse IP et le masque (donc le /XX) en binaire.\r\nReprenons l’adresse IP 192.168.3.1/24<br>\r\n\r\n192.168.3.1 			1100 0000.1010 1000.0000 0001.0000 0010<br>\r\n\r\n/24 correspond en binaire à 	1111 1111.1111 1111. 1111 1111.0000 0000<br>\r\n\r\nEnsuite nous appliquons l’opérateur logique ET <br>\r\n\r\nPetit rappel :	0 ET 0 = 0<br>\r\n\r\n0 ET 1 = 0<br>\r\n1 ET 0 = 0<br>\r\n\r\n1 ET 1 = 1<br>\r\n\r\n\r\nCe qui nous donne 1100 0000.1010 1000.0000 0001.0000 0000<br>\r\n\r\nQui est égal à 192.168.3.0<br>\r\n\r\n<br><br>\r\nComment calculer l’hôte ? <br>\r\n<br>\r\nDans un premier temps il faut convertir l’adresse IP et L’INVERSE du masque.<br>\r\n\r\nAdresse IP : 192.168.3.1/24<br>\r\n\r\n192.168.3.1 			1100 0000.1010 1000.0000 0001.0000 0010<br>\r\n\r\nL’INVERSE de /24		0000 0000.0000 0000.0000 0000.1111 1111<br>\r\n\r\nCe qui nous donne 		0000 0000.0000 0000.0000 0000.0000 0010<br>\r\n\r\nQui est égal à 0.0.0.1<br>\r\n\r\nEn conclusion <br>\r\n\r\nPour l\'hôte 192.168.3.1/24<br>\r\n\r\nL’adresse du réseau est : 		192.168.3.0<br>\r\n\r\nL’adresse de la première machine est : 	192.168.3.1<br>\r\n\r\nL’adresse de la dernière machine est : 	192.168.3.254<br>\r\n\r\nL’adresse de broadcast est : 		192.168.3.255<br>\r\n\r\n<br>\r\n\r\nPour approfondir<br>\r\n<br>\r\n\r\nUn exemple plus compliqué :<br>\r\n\r\n91.198.174.2/19<br>\r\n<br>\r\n\r\n\r\nLe masque de sous-réseau (/19) est 255.255.224.0<br>\r\n\r\nIP 91.198.174.2 : 	0101 1011.1100 0110.1010 1110.0000 0010<br>\r\n\r\nMasque :		1111 1111.1111 1111.1110 0000.0000 0000<br>\r\n\r\n=    			0101 1011.1100 0110.10100000.0000 0000<br>\r\n\r\nL’adresse du sous-réseau est donc : 91.198.160.0<br>\r\n<br>\r\n\r\n\r\nIP 91.198.174.2 : 	0101 1011.1100 0110.1010 1110.0000 0010<br>\r\n\r\nL’inverse du masque :  	0000 0000.0000 0000.0001 1111.1111 1111<br>\r\n\r\n= 			0000 0000.0000 0000.0000 1110.0000 0010<br>\r\n\r\nL’adresse de l’hôte est donc : 0.0.14.2<br>\r\n\r\nNous avons donc :<br>\r\n\r\nL’adresse du réseau est : 		91.198.160.0<br>\r\n\r\nL’adresse de la première machine est : 	91.198.160.1<br>\r\n\r\nL’adresse de la dernière machine est : 	91.198.191.254<br>\r\n\r\nL’adresse de broadcast est : 		91.198.191.255<br>\r\n\r\n\r\n\r\n\r\n\r\nComment calculer l’hôte ? <br>\r\n\r\nDans un premier temps il faut convertir l’adresse IP et L’INVERSE du masque.<br>\r\n\r\nAdresse IP : 192.168.3.1/24<br>\r\n\r\n192.168.3.1 			1100 0000.1010 1000.0000 0001.0000 0010<br>\r\n\r\nL’INVERSE de /24		0000 0000.0000 0000.0000 0000.1111 1111<br>\r\n\r\nCe qui nous donne 		0000 0000.0000 0000.0000 0000.0000 0010<br>\r\n\r\nQui est égal à 0.0.0.1<br>\r\n\r\nEn conclusion <br>\r\n\r\nPour l\'hôte 192.168.3.1/24<br>\r\n\r\nL’adresse du réseau est : 		192.168.3.0<br>\r\n\r\nL’adresse de la première machine est : 	192.168.3.1<br>\r\n\r\nL’adresse de la dernière machine est : 	192.168.3.254<br>\r\n\r\nL’adresse de broadcast est : 		192.168.3.255<br>\r\n\r\n<br>\r\n\r\nPour approfondir<br>\r\n<br>\r\n\r\nUn exemple plus compliqué :<br>\r\n\r\n91.198.174.2/19<br>\r\n<br>\r\n\r\n\r\nLe masque de sous-réseau (/19) est 255.255.224.0<br>\r\n\r\nIP 91.198.174.2 : 	0101 1011.1100 0110.1010 1110.0000 0010<br>\r\n\r\nMasque :		1111 1111.1111 1111.1110 0000.0000 0000<br>\r\n\r\n=    			0101 1011.1100 0110.10100000.0000 0000<br>\r\n\r\nL’adresse du sous-réseau est donc : 91.198.160.0<br>\r\n<br>\r\n\r\n\r\nIP 91.198.174.2 : 	0101 1011.1100 0110.1010 1110.0000 0010<br>\r\n\r\nL’inverse du masque :  	0000 0000.0000 0000.0001 1111.1111 1111<br>\r\n\r\n= 			0000 0000.0000 0000.0000 1110.0000 0010<br>\r\n\r\nL’adresse de l’hôte est donc : 0.0.14.2<br>\r\n\r\nNous avons donc :<br>\r\n\r\nL’adresse du réseau est : 		91.198.160.0<br>\r\n\r\nL’adresse de la première machine est : 	91.198.160.1<br>\r\n\r\nL’adresse de la dernière machine est : 	91.198.191.254<br>\r\n\r\nL’adresse de broadcast est : 		91.198.191.255<br>\r\n\r\n\r\n', 1),
(4, 'Les balises HTML', 'HTML : les balises<br><br>\r\n    HTML (HyperText Markup Language) est un langage descriptif utilisé pour structurer le contenu d\'une page (ses textes, ses images, ses liens, etc.).<br>\r\n    Un document HTML est un fichier texte qui contient des balises (ou tag en anglais). Ces balises doivent être utilisées d\'une certaine façon pour décrire correctement la structure du document.<br>\r\n    Les balises indiquent au navigateur comment afficher le document, certaines balises permettent d\'intégrer différents médias comme des images, des vidéos ou des musiques parmi le texte de la page.<br>\r\n<br><br>Balises de premier niveau<br><br>\r\n    Les balises de premier niveau sont les principales balises qui structurent une page HTML. Elles sont indispensables pour réaliser le « code minimal » d\'une page web.<br>\r\n	html : balise principale<br>\r\n	head : En-tête de la page<br>\r\n	body : Corps de la page<br>\r\n<br><br>\r\nBalises d\'en-tête<br><br>\r\n    Ces balises sont toutes situées dans l\'en-tête de la page web:<br>\r\n	link : liaison avec une feuille de style<br>\r\n	meta : métadonnées de la page web<br>\r\n	script : Code javascript<br>\r\n	style : Code CSS<br>\r\n	title : titre de la page<br>\r\n\r\n<br><br>Balises de formulaire<br><br>\r\n	form : formulaire<br>\r\n	fieldset : groupe de champs<br>\r\n	label : libellé d\'un champ<br>\r\n	input : champ de formulaire<br>\r\n	select : liste déroulante <br>\r\n	option : Elément d\'une liste déroulante<br>', 2);

-- --------------------------------------------------------

--
-- Structure de la table `resultat`
--

DROP TABLE IF EXISTS `resultat`;
CREATE TABLE IF NOT EXISTS `resultat` (
  `ID_EVAL` int(11) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `ID_RESULTAT` int(11) NOT NULL AUTO_INCREMENT,
  `RESULTAT` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ID_EVAL`,`ID_USER`,`ID_RESULTAT`),
  KEY `ID_EVAL` (`ID_EVAL`),
  KEY `ID_USER` (`ID_USER`),
  KEY `ID_RESULTAT` (`ID_RESULTAT`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `resultat`
--

INSERT INTO `resultat` (`ID_EVAL`, `ID_USER`, `ID_RESULTAT`, `RESULTAT`) VALUES
(1, 1, 3, '4'),
(1, 1, 5, '0'),
(1, 2, 1, '0'),
(1, 2, 2, '20'),
(8, 2, 4, '16');

-- --------------------------------------------------------

--
-- Structure de la table `theme`
--

DROP TABLE IF EXISTS `theme`;
CREATE TABLE IF NOT EXISTS `theme` (
  `ID_THEME` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ID_THEME`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `theme`
--

INSERT INTO `theme` (`ID_THEME`, `NOM`) VALUES
(1, 'Réseau'),
(2, 'Développement');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `ID_USER` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(255) NOT NULL,
  `PRENOM` varchar(255) NOT NULL,
  `PSEUDO` varchar(255) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `ADMIN` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID_USER`),
  UNIQUE KEY `PSEUDO` (`PSEUDO`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`ID_USER`, `NOM`, `PRENOM`, `PSEUDO`, `PASSWORD`, `ADMIN`) VALUES
(1, 'oui', 'oui', 'ouioui', 'oui', 0),
(2, 'non', 'non', 'non', 'non', 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `eval`
--
ALTER TABLE `eval`
  ADD CONSTRAINT `eval_ibfk_1` FOREIGN KEY (`ID_LESSON`) REFERENCES `lesson` (`ID_LESSON`);

--
-- Contraintes pour la table `lesson`
--
ALTER TABLE `lesson`
  ADD CONSTRAINT `lesson_ibfk_1` FOREIGN KEY (`ID_THEME`) REFERENCES `theme` (`ID_THEME`);

--
-- Contraintes pour la table `resultat`
--
ALTER TABLE `resultat`
  ADD CONSTRAINT `resultat_ibfk_1` FOREIGN KEY (`ID_EVAL`) REFERENCES `eval` (`ID_EVAL`),
  ADD CONSTRAINT `resultat_ibfk_2` FOREIGN KEY (`ID_USER`) REFERENCES `user` (`ID_USER`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
