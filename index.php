<?php

require_once "./connection_bdd.php";
session_start();
$PATH_TO_HEADER = "./assets/header/";
$PATH_TO_BODY = "./assets/body/";
$PATH_TO_FOOTER = "./assets/footer/";

$path_to_header = "";
$path_to_footer = "";
$access = "";


if (isset($_SESSION['id_user'])) {
    if ($_SESSION['access'] == 0) {
        $access = "user";
    } else if ($_SESSION['access'] == 1) {
        $access = "admin";
    }
} else {
    $access = "common";
}

$path_to_header = $PATH_TO_HEADER . $access . "_header.php";
$path_to_footer = $PATH_TO_FOOTER . $access . "_footer.php";

include_once $path_to_header;

if (isset($_GET['page'])) {
    $file = $_GET['page'] . "_" . $access . ".php";
} else {
    $file = 'index_body_' . $access . ".php";
}
$path = $PATH_TO_BODY . '/' . $file;

if (file_exists($path)) {
    include_once $path;
}else if ($_SERVER['REQUEST_URI'] != "/workshopmc/" && !isset($_GET["page"])) {
    header("Location: http://localhost/workshopmc/");
}else {
    include_once "./assets/body/error_404.php";
}

include_once $path_to_footer;
